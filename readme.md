# Datalog example

This repository contains an example datalog implementation of the generalization example from the 2015 TaPP [paper](http://cs.iit.edu/%7edbgroup/pdfpubls/GK15.pdf): **Towards Constraint-based Explanations for Answers and Non-Answers**
by Boris Glavic, Sven Koehler, Sean Riddle, and Bertram Ludaescher. The datalog implementation is for the examples shown in the paper. There is a single `.dlv` file (e.g., run with [DLV](http://www.dlvsystem.com/) using the commands given in the `.log` file).
