%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% DOC
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Encoding of 4 examples aorund the at most one transfer train connection database
% Balder-style most general explanation for why-not(paris,ny):
%     predicate: why_not
% Balder-style most general explanation for why(lyon,dijon)
%     predicate: why
% Our rule based most general explanation for why-not(paris,ny)
%     predicate: why_not_r0
%     predicate: why_not_r1
% Our rule based most general explanation for why(lyon,dijon)
%     predicate: why_r0
%     predicate: why_r1
% Our rule based most general explanation for why(ny,seattle)
%     predicate: why_2_r0
%	  predicate: why_2_r1

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% INSTANCE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
train(ny, washington_dc).
train(ny, chicago).
train(chicago, seattle).
train(washington_dc, seattle).
train(berlin, paris).
train(berlin, munich).
train(paris, lyon).
train(paris, dijon).
train(X,X) :- city(X).
train(X,Y) :- train(Y,X).

% cities and their states, countries, and continents
city(X) :- train(X,_).

city_state(chicago,illinois).
city_state(ny,nystate).
city_state(seattle,washington).
city_state(washington_dc,dc).

state_country(illinois,us).
state_country(nystate,us).
state_country(washington,us).
state_country(dc,us).

city_country(X,Y) :-
	city_state(X,Z), state_country(Z,Y).

city_country(berlin,germany).
city_country(munich,germany).
city_country(dijon,france).
city_country(lyon,france).
city_country(paris,france).

country(X) :- city_country(_,X).
country(X) :- state_country(_,X).

country_continent(us,northamerica).
country_continent(france,europe).
country_continent(germany,europe).

continent(X) :- country_continent(_,X).

% relation storing concepts
is_basic_concept(X) :- city(X).
is_concept(X) :- is_basic_concept(X).
is_concept(X) :- city_country(_,X).
is_concept(X) :- state_country(X,_).
is_concept(X) :- state_country(_,X).
is_concept(X) :- country_continent(_,X).
is_concept(a_city).

% relation storing subsumption of concepts
subsumes(X,Y) :- city_country(X,Y).
subsumes(X,Y) :- city_state(X,Y).
subsumes(X,Y) :- state_country(X,Y).
subsumes(X,Y) :- country_continent(X,Y).
subsumes(X,Y) :- subsumes(X,Z), subsumes(Z,Y).
subsumes(X,a_city) :- is_concept(X).
subsumes_equal(X,X) :- is_concept(X).
subsumes_equal(X,Y) :- subsumes(X,Y).

% find more general combinations of concepts
more_general_2(X,Y,Z,A) :- subsumes(X,Z), subsumes_equal(Y,A).
more_general_2(X,Y,Z,A) :- subsumes_equal(X,Z), subsumes(Y,A).

more_general_3(X,Y,Z,A,B,C) :- subsumes(X,A), subsumes_equal(Y,B), subsumes_equal(Z,C).
more_general_3(X,Y,Z,A,B,C) :- subsumes_equal(X,A), subsumes(Y,B), subsumes_equal(Z,C).
more_general_3(X,Y,Z,A,B,C) :- subsumes_equal(X,A), subsumes_equal(Y,B), subsumes(Z,C).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Implementation of Balder's definition (and its immediate extension to why) using DL only
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% queries/views and their negation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% train connection with one transfer stop
one_transfer(X,Y) :- train(X,Z), train(Z,Y).
one_transfer(X,Y) :- train(X,Y).

% no train connection with one transer
no_one_transfer(X,Y) :- is_basic_concept(X), is_basic_concept(Y), not one_transfer(X,Y).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% input is one_transfer rule and why_not() :- one_transfer(paris,ny).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generalize combinations of cities with no train connection: 
% 1) generalization of inputs, 2) no instance exists in the query result
exists_concept_one_transfer(X,Y) :- 
	subsumes_equal(Z,X), subsumes_equal(A,Y), one_transfer(Z,A).
gen_no_one_transfer(X,Y,Z,A) :- 
	subsumes_equal(Z,X), subsumes_equal(A,Y), 
	no_one_transfer(Z,A),
	not exists_concept_one_transfer(X,Y).

% more general combination
more_gen_no_one_transfer(A,B,X,Y) :- 
	gen_no_one_transfer(A,B,X,Y), 
	gen_no_one_transfer(C,D,X,Y), 
	more_general_2(A,B,C,D). 

% most general combination
most_gen_no_one_transfer(X,Y,Z,A) :- 
	gen_no_one_transfer(X,Y,Z,A), 
	not more_gen_no_one_transfer(X,Y,Z,A).

% why-not question
why_not(X,Y) :- most_gen_no_one_transfer(X,Y,paris,ny).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% input is one_transfer rule and why() :- one_transfer(lyon,dijon).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generalize combinations of cities with a train connection: 
% 1) generalization of inputs, 2) no instance that not exists
exists_concept_no_one_transfer(X,Y) :- 
	subsumes_equal(Z,X), subsumes_equal(A,Y), no_one_transfer(Z,A).
gen_one_transfer(X,Y,Z,A) :- 
	subsumes_equal(Z,X), subsumes_equal(A,Y), 
	one_transfer(Z,A), 
	not exists_concept_no_one_transfer(X,Y).

% more general combination
more_gen_one_transfer(A,B,X,Y) :- 
	gen_one_transfer(A,B,X,Y), gen_one_transfer(C,D,X,Y), more_general_2(A,B,C,D). 

% most general combination
most_gen_one_transfer(X,Y,Z,A) :- 
	gen_one_transfer(X,Y,Z,A), not more_gen_one_transfer(X,Y,Z,A).

% why question
why(X,Y) :- most_gen_one_transfer(X,Y,lyon,dijon).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% rules for rules and their negation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% rule binding for one_transfer(X,Y) :- train(X,Z), train(Z,Y).
r0_won(X,Y,Z) :- train(X,Z), train(Z,Y).
r1_won(X,Y) :- train(X,Y).

% lost rules
r0_lost(X,Y,Z) :- 
	is_basic_concept(X), is_basic_concept(Y), 
	is_basic_concept(Z), not r0_won(X,Y,Z).
r1_lost(X,Y) :- 
	is_basic_concept(X), is_basic_concept(Y), not r1_won(X,Y).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Our Rule binding idea for the why(lyon,dijon) question
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generalize rule bindings
exists_concept_r0_lost(X,Y,Z) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), subsumes_equal(C,Z), r0_lost(A,B,C).
gen_r0_won(X,Y,Z,A,B,C) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), subsumes_equal(C,Z), 
	r0_won(A,B,C), not exists_concept_r0_lost(X,Y,Z).

exists_concept_r1_lost(X,Y) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), r1_lost(A,B).
gen_r1_won(X,Y,A,B) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), 
	r1_won(A,B), not exists_concept_r1_lost(X,Y).

% more general rule bindings
more_gen_r0_won(A,B,C,X,Y,Z) :-
	gen_r0_won(A,B,C,X,Y,Z), gen_r0_won(D,E,F,X,Y,Z), more_general_3(A,B,C,D,E,F). 
more_gen_r1_won(A,B,X,Y) :-
	gen_r1_won(A,B,X,Y), gen_r1_won(C,D,X,Y), more_general_2(A,B,C,D). 

% most general rule bindings
most_gen_r0_won(X,Y,Z,A,B,C) :- 
	gen_r0_won(X,Y,Z,A,B,C), 
	not more_gen_r0_won(X,Y,Z,A,B,C).
most_gen_r1_won(X,Y,A,B) :- 
	gen_r1_won(X,Y,A,B), not more_gen_r1_won(X,Y,A,B).

% why with our semantics
why_r0(X,Y,Z) :-  
	most_gen_r0_won(X,Y,Z,lyon,dijon,_).
why_r1(X,Y) :-  
	most_gen_r1_won(X,Y,lyon,dijon).

% other why question (ny,seattle) with two most general explanations
why_2_r0(X,Y,Z) :-  
	most_gen_r0_won(X,Y,Z,ny,seattle,_).
why_2_r1(X,Y) :-  
	most_gen_r1_won(X,Y,ny,seattle).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Our Rule binding idea for the whynot(ny,paris) question
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% generalize rule bindings
exists_concept_r0_won(X,Y,Z) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), subsumes_equal(C,Z), r0_won(A,B,C).
gen_r0_lost(X,Y,Z,A,B,C) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), subsumes_equal(C,Z), 
	r0_lost(A,B,C), not exists_concept_r0_won(X,Y,Z).

exists_concept_r1_won(X,Y) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), r1_won(A,B).
gen_r1_lost(X,Y,A,B) :- 
	subsumes_equal(A,X), subsumes_equal(B,Y), r1_lost(A,B), not exists_concept_r1_won(X,Y).

% more general rule bindings
more_gen_r0_lost(A,B,C,X,Y,Z) :- 
	gen_r0_lost(A,B,C,X,Y,Z), gen_r0_lost(D,E,F,X,Y,Z), more_general_3(A,B,C,D,E,F). 
more_gen_r1_lost(A,B,X,Y) :- 
	gen_r1_lost(A,B,X,Y), gen_r1_lost(C,D,X,Y), more_general_2(A,B,C,D). 

% most general rule bindings
most_gen_r0_lost(X,Y,Z,A,B,C) :- 
	gen_r0_lost(X,Y,Z,A,B,C), not more_gen_r0_lost(X,Y,Z,A,B,C).
most_gen_r1_lost(X,Y,A,B) :- 
	gen_r1_lost(X,Y,A,B), not more_gen_r1_lost(X,Y,A,B).

% why-not question
why_not_r0(X,Y,Z) :- most_gen_r0_lost(X,Y,Z,paris,ny,_).
why_not_r1(X,Y) :- most_gen_r1_lost(X,Y,paris,ny).

